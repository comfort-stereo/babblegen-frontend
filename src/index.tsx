import * as React from "react"
import * as ReactDOM from "react-dom"
import { View } from "./app/view/view"
import registerServiceWorker from "./registerServiceWorker"

import "bootswatch/superhero/bootstrap.min.css"
import "./app/style/index.css"
import { Speaker } from "./app/speech/speaker"
import { O } from "omnitool"

O.run(() => {
    Speaker.initialize()
})

/**
 * Render the application.
 */
function render(element: any) {
    ReactDOM.render(element, document.getElementById("root") as HTMLElement)
}

render(<View/>)

/*
 * This enables hot reloading of React components on the development server.
 */
if (module.hot) {
    module.hot.accept("./app/view/view", () => {
        const View = require("./app/view/view").View
        render(<View/>)
    })
}

registerServiceWorker()
