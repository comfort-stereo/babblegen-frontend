import { Source, SourceState } from "./source"
import { action, computed, observable, runInAction } from "mobx"
import { O } from "omnitool"

export class ExternalSourceStore {
    @observable data = null as string[] | null
    @observable state = SourceState.Empty
    @observable target = ""
}

const updateDelay = 1000

/**
 * Interface for a source pointing to an 'username-like' target on a website.
 */
export abstract class ExternalSource implements Source {
    store = new ExternalSourceStore()

    abstract title: string
    abstract icon: string
    abstract displayTarget: string

    private lastEditTime = O.now()
    private editID = 0

    get preview(): string {
        return this.displayTarget
    }

    @computed
    get state(): SourceState {
        return this.store.state
    }

    set state(value: SourceState) {
        this.store.state = value
    }

    @computed
    get data(): string[] | null {
        return this.store.data
    }

    set data(value: string[] | null) {
        this.store.data = value
    }

    @computed
    get target(): string {
        return this.store.target
    }

    set target(value: string) {
        this.store.target = value
        this.store.state = O.some(this.store.target) ? SourceState.Changed : SourceState.Empty
        this.lastEditTime = O.now()
        this.editID++
    }

    @action
    tick() {
        const state = this.store.state
        if (state !== SourceState.Changed) {
            return
        }
        if (O.now() - this.lastEditTime > updateDelay) {
            this.store.state = SourceState.Loading
            this.load()
        }
    }

    @action
    async load() {
        try {
            this.store.state = SourceState.Loading
            this.store.data = null
            const id = this.editID
            const data = await this.fetch()
            runInAction(() => {
                if (id === this.editID) {
                    this.store.data = data
                    this.store.state = SourceState.Ready
                }
            })
        } catch (error) {
            runInAction(() => {
                this.store.state = SourceState.Error
            })
        }
    }

    protected abstract async fetch(): Promise<string[]>
}
