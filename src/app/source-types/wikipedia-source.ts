import { ExternalSource } from "./external-source"
import { Backend } from "../backend/backend"
import { Resources } from "../configuration/resources"

/**
 * Text generation source pointing to a Wikipedia article.
 */
export class WikipediaSource extends ExternalSource {

    get title(): string {
        return "Article"
    }

    get displayTarget(): string {
        return this.target
    }

    get icon(): string {
        return Resources.wikipediaLogo
    }

    protected async fetch(): Promise<string[]> {
        return Backend.getWikipediaArticle(this.target)
    }
}
