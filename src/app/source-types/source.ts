export enum SourceState {
    Empty,
    Changed,
    Loading,
    Ready,
    Error
}

export interface Source {

    /**
     *
     */
    title: string

    /**
     *
     */
    preview: string

    /**
     * Path to the icon for the source type.
     */
    icon: string

    /**
     *
     */
    data: string[] | null

    /**
     *
     */
    state: SourceState

    /**
     * Fetch the source, possibly asyncronously from its origin.
     */
    load(): void

    tick(): void
}
