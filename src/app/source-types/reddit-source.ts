import { Resources } from "../configuration/resources"
import { Backend } from "../backend/backend"
import { ExternalSource } from "./external-source"

/**
 * Text generation source pointing to a Reddit subreddit.
 */
export class RedditSource extends ExternalSource {

    constructor(target: string = "") {
        super()
        this.target = target
    }

    get title(): string {
        return "Subreddit"
    }

    get displayTarget(): string {
        return "r/" + this.target
    }

    get icon(): string {
        return Resources.redditLogo
    }

    protected async fetch(): Promise<string[]> {
        return Backend.getRedditSubreddit(this.target)
    }
}
