import { ExternalSource } from "./external-source"
import { Backend } from "../backend/backend"
import { Resources } from "../configuration/resources"

/**
 * Text generation source pointing to a Twitter account.
 */
export class TwitterSource extends ExternalSource {

    get title(): string {
        return "Account"
    }

    get displayTarget(): string {
        return "@" + this.target
    }

    get icon(): string {
        return Resources.twitterLogo
    }

    protected async fetch(): Promise<string[]> {
        return Backend.getTwitterUser(this.target)
    }
}
