import { Configuration } from "../configuration/configuration"
import { O } from "omnitool"

/**
 * Methods for communicating with the backend server.
 */
export namespace Backend {

    const errorMessage = "Failed to fetch sources(s)."

    /**
     * Retrieves tweets from a Twitter user.
     */
    export async function getTwitterUser(username: string): Promise<string[]> {
        return get(`/api/twitter/user/${username}`)
    }

    /**
     * Retrieves posts from a Reddit subreddit.
     */
    export async function getRedditSubreddit(subreddit: string): Promise<string[]> {
        return get(`/api/reddit/subreddit/${subreddit}`)
    }

    /**
     * Retrieves body text from a Wikipedia article.
     */
    export async function getWikipediaArticle(article: string): Promise<string[]> {
        return get(`/api/wikipedia/article/${article}`)
    }

    /**
     * Retrieves text data from the backend with a provided path.
     */
    async function get(path: string): Promise<string[]> {
        const urls = [
            Configuration.localURL,
            Configuration.backendURL
        ]

        let response: Response | null = null

        for (const url of urls) {
            try {
                response = await fetch(url + path)
                break
            } catch (error) {
                response = null
            }
        }

        if (response === null) {
            throw new Error()
        }

        try {
            const json = await response.json()
            if (O.isArray(json) && O.some(json)) {
                return O.filter(json, O.exists).done()
            }
        } catch (error) {
            throw new Error(errorMessage)
        }

        throw new Error(errorMessage)
    }
}