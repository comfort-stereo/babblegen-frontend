import { Source } from "../../source-types/source"
import { RedditSource } from "../../source-types/reddit-source"
import { action, computed, observable } from "mobx"
import { O } from "omnitool"

class Store {
    @observable sources: Source[] = [new RedditSource("askreddit")]
    @observable index: number = 0
}

export const maximumSourceCount = 3

export class SourceContext {
    private readonly store = new Store()

    constructor() {
        setInterval(() => {
            O.each(this.sources, (source) => source.tick())
        }, 25)
    }

    @computed
    get index(): number {
        return this.store.index
    }

    set index(value: number) {
        this.store.index = O.clamp(value, 0, O.count(this.store.sources) - 1)
    }

    @computed
    get full(): boolean {
        return O.count(this.store.sources) === maximumSourceCount
    }

    get sources(): Iterable<Source> {
        return O.take(this.store.sources)
    }

    @computed
    get sourceCount(): number {
        return O.count(this.store.sources)
    }

    get<T extends Source>(index: number): T {
        return O.at(this.store.sources, index) as T
    }

    @action
    add(source: Source) {
        if (this.sourceCount < maximumSourceCount) {
            this.store.sources = [...this.store.sources, source]
            this.store.index = O.count(this.store.sources) - 1
        }
    }

    @action
    set(source: Source) {
        this.store.sources = O.set(this.store.sources, this.store.index, source).done()
    }

    @action
    remove() {
        if (O.count(this.store.sources) > 1) {
            this.store.sources = O.cut(this.store.sources, this.store.index).done()
            this.store.index = O.clamp(this.store.index - 1, 0, O.count(this.store.sources) - 1)
        }
    }
}
