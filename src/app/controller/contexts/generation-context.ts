import { Generator } from "../../text-generation/generator"
import { SourceContext } from "./source-context"
import { AudioContext } from "./audio-context"
import { action, autorun, computed, observable, runInAction } from "mobx"
import { SourceState } from "../../source-types/source"
import { O } from "omnitool"

export enum GenerationState {
    None,
    Loading,
    Building,
    Ready,
    Error
}

class Store {
    @observable output = ""
    @observable sentenceCount = 1
    @observable building = false
}

export const doubleWordProbability = 0.65
export const minimumSentenceCount = 1
export const maximumSentenceCount = 50

export class GenerationContext {
    private readonly store = new Store()
    private readonly source: SourceContext
    private readonly audio: AudioContext

    private generator: Generator | null = null

    constructor(source: SourceContext, audio: AudioContext) {
        this.source = source
        this.audio = audio
        autorun(() => {
            if (this.state !== GenerationState.Ready) {
                this.reset()
            }
        })
    }

    @computed
    get sentenceCount(): number {
        return this.store.sentenceCount
    }

    set sentenceCount(count: number) {
        const [min, max] = [
            minimumSentenceCount,
            maximumSentenceCount
        ]
        this.store.sentenceCount = O.clamp(count, min, max)
    }

    @computed
    get state(): GenerationState {
        const sources = [...this.source.sources]
        if (this.store.building) {
            return GenerationState.Building
        }
        if (O.all(sources, ({state}) => state === SourceState.Ready)) {
            return GenerationState.Ready
        }
        if (O.some(sources, ({state}) => state === SourceState.Error)) {
            return GenerationState.Error
        }
        if (O.some(sources, ({state}) => state === SourceState.Loading || state === SourceState.Changed)) {
            return GenerationState.Loading
        }
        return GenerationState.None
    }

    @computed
    get output(): string {
        return this.store.output
    }

    @action
    generate() {
        if (this.state === GenerationState.Ready) {
            O.run(async () => {
                this.sentenceCount = O.high(this.sentenceCount, 1)
                const generator = this.generator || await this.build()
                runInAction(() => {
                    this.store.output = generator.generateSentences(this.sentenceCount)
                    this.audio.speak(this.store.output)
                })
            })
        }
    }

    @action
    reset() {
        this.generator = null
        this.store.output = ""
    }

    @action
    private async build(): Promise<Generator> {
        this.store.building = true
        const probability = doubleWordProbability
        await O.delay(50)
        this.generator = new Generator(() => O.chance(probability) ? 2 : 1)
        for (const source of this.source.sources) {
            const {data} = source
            if (O.exists(data)) {
                this.generator.consume(data)
            }
        }
        runInAction(() => {
            this.store.building = false
        })
        return this.generator
    }
}
