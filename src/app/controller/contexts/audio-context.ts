import { computed, observable } from "mobx"
import { Speaker } from "../../speech/speaker"
import { O } from "omnitool"

class Store {
    @observable speaking = false
    @observable playingMusic = false
    @observable musicVolume = 0.85
}

export class AudioContext {
    private readonly store = new Store()

    get speechIsAvailable(): boolean {
        return Speaker.available
    }

    @computed
    get speaking(): boolean {
        return this.store.speaking
    }

    set speaking(value: boolean) {
        if (this.speechIsAvailable && value) {
            this.store.speaking = true
        } else {
            Speaker.stop()
            this.store.speaking = false
        }
    }

    @computed
    get playingMusic(): boolean {
        return this.store.playingMusic
    }

    set playingMusic(value: boolean) {
        this.store.playingMusic = value
        if (value) {
            this.element.play()
        } else {
            this.element.pause()
        }
    }

    @computed
    get musicVolume(): number {
        return this.store.musicVolume
    }

    set musicVolume(value: number) {
        const volume = O.clamp(value, 0, 1)
        this.store.musicVolume = volume
        this.element.volume = volume
    }

    private _element: HTMLAudioElement | null = null

    private get element(): HTMLAudioElement {
        const element = this._element || (this._element = getAudioElement())
        element.volume = this.store.musicVolume
        return element
    }

    speak(string: string) {
        if (this.store.speaking) {
            Speaker.speak(string)
        }
    }
}

function getAudioElement(): HTMLAudioElement {
    return O.unpack(document.getElementById("music")) as HTMLAudioElement
}
