import { action, computed, observable } from "mobx"
import { GenerationContext } from "./generation-context"

class Store {
    @observable showOutput: boolean
}

export class UIContext {
    private readonly store = new Store()

    private generation: GenerationContext

    constructor(generation: GenerationContext) {
        this.store.showOutput = false
        this.generation = generation
        onkeypress = (event) => {
            this.onKeyDown(event)
        }
    }

    @computed
    get showOutput(): boolean {
        return this.store.showOutput
    }

    set showOutput(value: boolean) {
        this.store.showOutput = value
    }

    @action
    toggleOutput() {
        this.store.showOutput = !this.store.showOutput
    }

    @action
    onKeyDown(event: KeyboardEvent) {
        const code = event.keyCode
        if (code === 27) {                  /* Escape */
            this.store.showOutput = false
        } else if (code === 39) {           /* Right Arrow */
            if (this.store.showOutput) {
                this.generation.generate()
            }
        }
    }
}
