import { AudioContext } from "./contexts/audio-context"
import { GenerationContext } from "./contexts/generation-context"
import { SourceContext } from "./contexts/source-context"
import { UIContext } from "./contexts/ui-context"
import { useStrict } from "mobx"

/**
 * Singleton that contains the application's actions and state. Divided into contexts.
 */
export class Controller {
    static readonly instance = new Controller()

    readonly source = new SourceContext()
    readonly audio = new AudioContext()
    readonly generation = new GenerationContext(this.source, this.audio)
    readonly ui = new UIContext(this.generation)

    private constructor() {
        useStrict(true)
    }
}

/**
 * Function to access the controller globally.
 */
export function app() {
    return Controller.instance
}
