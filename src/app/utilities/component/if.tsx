import * as React from "react"

/**
 * Component that renders its children given a provided condition is true.
 */
export const If = (props) => {
    if (props.condition) {
        return props.children
    }
    return <span/>
}
