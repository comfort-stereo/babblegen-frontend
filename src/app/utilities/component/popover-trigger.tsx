import * as React from "react"
import { observer } from "mobx-react"
import { OverlayTrigger, Popover } from "react-bootstrap"

export const PopoverTrigger = observer(({message, placement, children}: {
    message?: any
    placement: "left" | "right" | "top" | "bottom"
    children?: any
}) => {
    return <OverlayTrigger trigger="hover"
                           placement={placement}
                           delayShow={500}
                           overlay={
                               <Popover className="fade-in">
                                   {message}
                               </Popover>
                           }>
        {children}
    </OverlayTrigger>
})
