import * as React from "react"

export class Reloader<T> extends React.Component<{ content: T, children?: any }, { previous: T, show: boolean }> {
    constructor(props: { content: T }) {
        super(props)
        this.state = {
            show: true,
            previous: props.content
        }
    }

    render() {
        const {content, children} = this.props
        const {previous, show} = this.state

        if (content !== previous) {
            this.setState({
                show: false,
                previous: content
            })
            setTimeout(() => {
                this.setState({show: true})
            })
        }

        return show ? children : null
    }
}
