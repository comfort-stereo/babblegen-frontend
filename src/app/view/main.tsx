import "../style/index.css"

import * as React from "react"
import {
    Button,
    ButtonGroup,
    ControlLabel,
    DropdownButton,
    Form,
    FormGroup,
    Glyphicon,
    MenuItem
} from "react-bootstrap"
import { app } from "../controller/controller"
import { observer } from "mobx-react"
import { Spacer } from "../utilities/component/spacer"
import { O } from "omnitool"
import { SourcePreview, SourceView } from "./source-view"
import { Resources } from "../configuration/resources"
import { RedditSource } from "../source-types/reddit-source"
import { TwitterSource } from "../source-types/twitter-source"
import { WikipediaSource } from "../source-types/wikipedia-source"
import { OutputView } from "./output-view"
import { Source, SourceState } from "../source-types/source"
import { Music } from "./music"
import { UIUtility } from "../utilities/react-ut"
import { PopoverTrigger } from "../utilities/component/popover-trigger"

export const Main = observer(() => {
    return <div className="main">
        <Top/>
        <UpperControls/>
        <Content/>
        <LowerControls/>
    </div>
})

const Top = observer(() => {
    return <div>
        <Music/>
    </div>
})

const UpperControls = observer(() => {
    return <div>
        <ControlLabel>
            Source
        </ControlLabel>
        <Spacer height={10}/>
        <SourceTabs/>
        <ManipulatorButtons/>
        <Spacer height={10}/>
    </div>
})

const SourceTabs = observer(() => {
    return <ButtonGroup justified>
        {[...O.range(app().source.sourceCount).map((i) => {
            const source = app().source.get(i)
            const style = sourceStateToStyle(source.state)
            const selected = app().source.index === i

            const message = selected 
                ? "You are editing this source."
                : "Click to edit."

            return <PopoverTrigger placement="top" message={message}>
                <ButtonGroup>
                    <Button bsStyle={style}
                            value={i}
                            onClick={() => {
                                app().source.index = i
                                app().ui.showOutput = false
                            }}>
                        <SourcePreview index={i}/>
                    </Button>
                </ButtonGroup>
            </PopoverTrigger>
        })]}
    </ButtonGroup>
})

const ManipulatorButtons = observer(() => {
    const style = UIUtility.onMobile ? {} : {
        display: "inline-block",
        width: "34%"
    }
    return <ButtonGroup justified
                        style={style}>
        <AddSourceButton/>
        <ChangeSourceButton/>
        <RemoveSourceButton/>
    </ButtonGroup>
})

const dropdownEntryData: [{ new(): Source }, string][] = [
    [RedditSource, Resources.redditLogo],
    [TwitterSource, Resources.twitterLogo],
    [WikipediaSource, Resources.wikipediaLogo]
]

const AddSourceButton = observer(() => {
    const message = "Add a new social media source."
    const disabled = app().source.full
    const icon = <Glyphicon style={{fontSize: 20}} glyph={"plus-sign"}/>

    const button = <DropdownButton disabled={disabled} 
                                   id="change-source-dropdown"
                                   title={icon as any}
                                   style={{
                                       borderBottomLeftRadius: 20,
                                       borderBottomRightRadius: 20
                                   }}>
        {[...O.map(dropdownEntryData, ([type, icon]) => {
            return <DropdownButtonEntry icon={icon}
                                        onClick={() => {
                                            app().source.add(new type())
                                            app().ui.showOutput = false
                                        }}/>
        })
        ]}
    </DropdownButton>

    /*
     * @workaround
     * 
     * This is a workaround for a react bootstrap bug where the popover stays open forever when the button is disabled.
     */
    return disabled ? button : <PopoverTrigger message={message} 
                                               placement="left">
        {button}
    </PopoverTrigger>
})

const ChangeSourceButton = observer(() => {
    const icon = <Glyphicon style={{fontSize: 20}}
                            glyph="new-window"/>

    const message = "Change the type of the selected source."
    return <PopoverTrigger message={message}
                           placement="bottom">
        <DropdownButton id="change-source-dropdown"
                        title={icon as any}
                        style={{
                            borderBottomLeftRadius: 20,
                            borderBottomRightRadius: 20
                        }}>
            {[...O.map(dropdownEntryData, ([type, icon]) => {
                return <DropdownButtonEntry icon={icon}
                                            onClick={() => {
                                                app().source.set(new type())
                                                app().ui.showOutput = false
                                            }}/>
            })
            ]}
        </DropdownButton>
    </PopoverTrigger>
})

const DropdownButtonEntry = ({icon, onClick}: {
    icon: string,
    onClick: () => void
}) => {
    return <MenuItem>
        <Button bsSize={"xs"}
                bsStyle="success"
                onClick={onClick}>
            <img src={icon}
                 style={{height: 30}}/>
        </Button>
    </MenuItem>
}

const RemoveSourceButton = observer(() => {
    const message = "Remove the selected source."
    const disabled = app().source.sourceCount < 2
    const button = <ButtonGroup>
        <Button disabled={disabled}
                onClick={() => {
                    app().source.remove()
                    app().ui.showOutput = false
                }}
                style={{
                    borderBottomLeftRadius: 20,
                    borderBottomRightRadius: 20
                }}>
            <Glyphicon style={{fontSize: 20}}
                       glyph={"remove-circle"}/>
        </Button>
    </ButtonGroup>

    /*
     * @workaround
     * 
     * This is a workaround for a react bootstrap bug where the popover stays open forever when the button is disabled.
     */
    return disabled ? button : <PopoverTrigger message={message} 
                                               placement="right">
        {button}
    </PopoverTrigger>
})

const Content = observer(() => {
    const style = UIUtility.onMobile ? {
        height: 300
    } : {
        height: 400
    }
    return <div style={style}>
        {
            app().ui.showOutput ?
                <OutputView/> :
                <SourceView index={app().source.index}/>
        }
    </div>
})

const LowerControls = observer(() => {
    const space = UIUtility.onMobile ? <Spacer height={10}/> : <hr/>
    return <FormGroup>
        <Form inline onSubmit={(event) => event.preventDefault()}>
            {space}
            <GenerateButton/>
            <br/>
            <ToggleOutputButton/>
        </Form>
    </FormGroup>
})

const GenerateButton = observer(() => {
    const style = UIUtility.onMobile ? {width: "100%"} : {width: "60%"}
    const message = "Generate commentary based on the current sources."
    return <PopoverTrigger message={message}
                           placement="right">
        <Button bsSize="lg"
                       bsStyle="success"
                       onClick={() => {
                           app().ui.showOutput = true
                           app().generation.generate()
                       }}
                       style={{
                           ...style,
                           borderRadius: 100
                       }}>
            <Glyphicon glyph="repeat"
                       style={{
                           fontSize: 16
                       }}/>
            <Spacer width={10}/>
            Generate
        </Button>
    </PopoverTrigger>
})

const ToggleOutputButton = observer(() => {
    const style = UIUtility.onMobile ? {width: "60%"} : {width: "30%"}
    const message = "Switch between the output view and source editor."
    return <PopoverTrigger message={message}
                           placement="left">
        <Button onClick={() => app().ui.showOutput = !app().ui.showOutput}
                       style={{
                           ...style,
                           borderBottomLeftRadius: 20,
                           borderBottomRightRadius: 20
                       }}>
            <Glyphicon glyph="retweet"
                       style={{fontSize: 10}}/>
            <Spacer width={10}/>
            Toggle Output
        </Button>
    </PopoverTrigger>
})

function sourceStateToStyle(state: SourceState): string {
    if (state === SourceState.Empty) {
        return "info"
    } else if (state === SourceState.Loading || state === SourceState.Changed) {
        return "warning"
    } else if (state === SourceState.Ready) {
        return "success"
    } else if (state === SourceState.Error) {
        return "danger"
    }
    return "default"
}
