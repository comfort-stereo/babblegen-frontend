import * as React from "react"
import { Resources } from "../configuration/resources"
import { observer } from "mobx-react"

export const Music: () => JSX.Element = observer(() => {
    return <div style={{display: "none"}}>
        <audio id="music" loop>
            <source src={Resources.music} type="audio/ogg"/>
        </audio>
    </div>
})
