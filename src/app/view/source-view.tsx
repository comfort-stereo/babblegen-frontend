import "../style/index.css"
import * as React from "react"
import { ControlLabel, Form, FormControl, FormGroup, Glyphicon } from "react-bootstrap"
import { app } from "../controller/controller"
import { Resources } from "../configuration/resources"
import { Spacer } from "../utilities/component/spacer"
import { UIUtility } from "../utilities/react-ut"
import { observer } from "mobx-react"
import { ExternalSource } from "../source-types/external-source"
import { If } from "../utilities/component/if"
import { O } from "omnitool"
import { SourceState } from "../source-types/source"

export const SourceView = observer(({index}: {
    index: number
}) => {
    const source = app().source.get(index)
    if (source instanceof ExternalSource) {
        return <ExternalSourceView index={index}/>
    }
    return <div/>
})

export const SourcePreview = observer(({index}: {
    index: number
}) => {
    const selected = app().source.index === index
    const source = app().source.get(index)
    const {icon, preview, state} = source

    return <span>
        <SourcePreviewIcon image={icon}/>
        <Spacer width={10}/>
        {preview}
        <Spacer width={10}/>
        <If condition={selected}>
            <Glyphicon glyph="pencil"/>
        </If>
        <If condition={state === SourceState.Loading || state === SourceState.Changed}>
            <Spacer width={10}/>
            <span className="spin">
                <Glyphicon glyph="repeat"/>
            </span>
        </If>
    </span>
})

const ExternalSourceView = observer(({index}: {
    index: number
}) => {
    const source = app().source.get(index)
    const {icon, title} = source
    const style = UIUtility.onMobile ? {width: "100%"} : {width: "75%"}

    return <div className="source-view" style={style}>
        <SourceViewIcon image={icon}/>
        <Spacer width={10}/>
        <FormGroup>
            <Form onSubmit={(event) => {
                event.preventDefault()
                app().ui.showOutput = true
                app().generation.generate()
            }}>
                <Spacer height={30}/>
                <ControlLabel>
                    {title}
                </ControlLabel>
                <br/>
                <ExternalSourceTargetForm index={index}/>
            </Form>
        </FormGroup>
    </div>
})

const ExternalSourceTargetForm = observer(({index}: {
    index: number
}) => {
    const source = app().source.get(index) as ExternalSource
    const {target, displayTarget} = source
    const style = UIUtility.onMobile ? {width: "80%"} : {width: "50%"}

    return <FormControl value={displayTarget}
                        type="text"
                        spellCheck={false}
                        style={{
                            ...style,
                            display: "inline-block"
                        }}
                        onChange={(event) => {
                            const text = UIUtility.getTargetValue(event)
                            source.target = O.drop(text, O.count(displayTarget) - O.count(target)).join()
                        }}/>
})

const SourceViewIcon = observer(({image}: {
    image: string
}) => {
    return <img src={image}
                style={{
                    height: 38,
                    paddingLeft: 5
                }}/>
})

const SourcePreviewIcon = observer(({image}: {
    image: string
}) => {
    return <img src={image}
                style={{
                    height: 30,
                    paddingBottom: 5
                }}/>
})
