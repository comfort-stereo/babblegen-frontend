import { Col, Grid, Row } from "react-bootstrap"
import * as React from "react"
import { Main } from "./main"
import { Configuration } from "../configuration/configuration"
import { observer } from "mobx-react"
import { UIUtility } from "../utilities/react-ut"
import { Spacer } from "../utilities/component/spacer"

/**
 * Main UI element of the application.
 */
export const View = observer(() => {
    return <div className="app">
        <Title/>
        <Header/>
        <Body/>
        <Spacer height={100}/>
        <Footer/>
    </div>
})

const Title = observer(() => {
    return <title>
        {Configuration.appName}
    </title>
})

const Header = observer(() => {
    return <div className="header">
        <h2>
            {Configuration.appName}
        </h2>
    </div>
})

const Body = observer(() => {
    return UIUtility.onMobile ? <Main/> : <Grid fluid>
        <Row>
            <Col xs={0} md={2}/>
            <Col xs={12} md={8}>
                <Main/>
            </Col>
            <Col xs={0} md={2}/>
        </Row>
    </Grid>
})

const Footer = observer(() => {
    return <div className="footer"/>
})
