import * as React from "react"
import { observer } from "mobx-react"
import { app } from "../controller/controller"
import { If } from "../utilities/component/if"
import { Reloader } from "../utilities/component/reloader"
import { GenerationState, maximumSentenceCount, minimumSentenceCount } from "../controller/contexts/generation-context"
import { Button, ControlLabel, Glyphicon } from "react-bootstrap"
import { Spacer } from "../utilities/component/spacer"
import * as ButtonGroup from "react-bootstrap/lib/ButtonGroup"
import { UIUtility } from "../utilities/react-ut"
import { PopoverTrigger } from "../utilities/component/popover-trigger"
import { O } from "omnitool"

export const OutputView = observer(() => {
    const {speechIsAvailable} = app().audio
    return <div>
        <SentenceCountControls/>
        <OutputViewText/>
        <If condition={speechIsAvailable}>
            <AudioControls/>
        </If>
    </div>
})

const largeSentenceCountIncrement = 5

const SentenceCountControls = observer(() => {
    const {sentenceCount} = app().generation
    const radius = 50
    const paddingTop = 3
    return <div className="sentence-count-controls">
        <Button bsSize="xs"
                style={{
                    borderRadius: radius,
                    paddingTop: paddingTop
                }}
                onClick={() => {
                    const increment = largeSentenceCountIncrement
                    app().generation.sentenceCount = sentenceCount - increment - sentenceCount % increment
                }}>
            <Glyphicon glyph="menu-left" style={{fontSize: 16}}/>
            <Spacer width={1}/>
        </Button>
        <Button bsSize="xs"
                style={{
                    borderBottomLeftRadius: radius,
                    borderTopLeftRadius: radius,
                    paddingTop: paddingTop,
                }}
                onClick={() => app().generation.sentenceCount--}>
            <Glyphicon glyph="menu-left" style={{fontSize: 16}}/>
            <Spacer width={20}/>
        </Button>
        <Button bsSize="xs"
                bsStyle="success"
                style={{
                    borderRadius: 2,
                    width: 40
                }}
                onClick={() => {
                    app().generation.sentenceCount = sentenceCount === minimumSentenceCount
                        ? maximumSentenceCount
                        : minimumSentenceCount
                }}>
                <span style={{fontSize: 16}}>
                    {sentenceCount}
                </span>
        </Button>
        <Button bsSize="xs"
                style={{
                    borderBottomRightRadius: radius,
                    borderTopRightRadius: radius,
                    paddingTop: paddingTop
                }}
                onClick={() => app().generation.sentenceCount++}>
            <Spacer width={20}/>
            <Glyphicon glyph="menu-right" style={{fontSize: 16}}/>
        </Button>
        <Button bsSize="xs"
                style={{
                    borderRadius: radius,
                    paddingTop: paddingTop
                }}
                onClick={() => {
                    const increment = largeSentenceCountIncrement
                    app().generation.sentenceCount = sentenceCount + increment - sentenceCount % increment
                }}>
            <Spacer width={1}/>
            <Glyphicon glyph="menu-right" style={{fontSize: 16}}/>
        </Button>
        <Spacer height={2}/>
        <ControlLabel>
            Sentences
        </ControlLabel>
    </div>
})

const OutputViewText = observer(() => {
    const {output, state} = app().generation
    const style = UIUtility.onMobile ? {
        height: 150,
        fontSize: 21
    } : {
        height: 250,
        fontSize: 26
    }
    return <div className="output-view" style={style}>
        <Reloader content={output + " " + state}>
            <If condition={state === GenerationState.Ready}>
                <div className="output-view-text">
                    {output ? output : "Ready"}
                </div>
            </If>
            <If condition={state !== GenerationState.Ready}>
                <div className="output-view-text">
                    <If condition={state === GenerationState.Loading}>
                        Fetching Sources
                    </If>
                    <If condition={state === GenerationState.Error}>
                        Could Not Fetch Sources
                    </If>
                    <If condition={state === GenerationState.Building}>
                        Building Generator
                    </If>
                    <If condition={state === GenerationState.None}>
                        Some Sources are Blank
                    </If>
                </div>
            </If>
        </Reloader>
    </div>
})

const AudioControls = observer(() => {
    const {showOutput} = app().ui
    const {speaking, playingMusic} = app().audio
    const {output} = app().generation
    const radius = 20
    return <div>
        <Spacer height={20}/>
        <ButtonGroup>
            <ButtonGroup>
                <SpeakButton/>
                <MusicButton/>
            </ButtonGroup>
        </ButtonGroup>
     </div>

})

const SpeakButton = observer(() => {
    const {speaking} = app().audio
    const {showOutput} = app().ui
    const {output} = app().generation
    const radius = 20

    const message = O.join([
        "Speak the output text using a voice synthesizer.",
        "This is not supported in all browsers at the moment.",
        "Sorry!"
    ], " ")

    return <PopoverTrigger placement="left" message={message}>
        <Button bsStyle={speaking ? "success" : "default"}
            style={{
                borderTopLeftRadius: radius,
                borderBottomLeftRadius: radius,
            }}
            onClick={() => {
                app().audio.speaking = !speaking
                if (output && showOutput) {
                    app().audio.speak(output)
                }
            }}>
            Speak
        </Button>
    </PopoverTrigger>
})

const MusicButton = observer(() => {
    const {playingMusic} = app().audio
    const radius = 20

    const message = "Set the mood, have some tea."

    return <PopoverTrigger placement="right" message={message}>
        <ButtonGroup>
            <Button bsStyle={playingMusic ? "success" : "default"}
                    style={{
                        borderTopRightRadius: radius,
                        borderBottomRightRadius: radius,
                    }}
                    onClick={() => {
                        app().audio.playingMusic = !playingMusic
                    }}>
                Music
            </Button>
        </ButtonGroup>
    </PopoverTrigger>
})
