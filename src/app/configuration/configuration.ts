export namespace Configuration {
    /**
     * Name of the application, used for display purposes.
     */
    export const appName = "Babblegen"

    /**
     * URL of the backend server.
     */
    export const backendURL = "https://babblegen.herokuapp.com"

    /**
     * URL of the backend server.
     */
    export const localURL = "http://localhost:5000"
}