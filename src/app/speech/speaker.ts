import { O } from "omnitool"

export class Speaker {

    /**
     * Reference to the browser's speech synthesis API.
     */
    private static synthesis: SpeechSynthesis = window.speechSynthesis

    /**
     * Returns true if the speech synthesis is available.
     */
    static get available(): boolean {
        return true
    }

    /**
     * Called when the application starts to ensure that speech synthesis voices are immediately loaded.
     */
    static initialize() {
        if (Speaker.available) {
            Speaker.synthesis.getVoices()
        }
    }

    /**
     * Start speaking a message. Stops speaking any text previously being read.
     */
    static speak(message: string) {
        if (Speaker.available) {
            this.stop()
            const chunks = message.split(/[;,.]/)
            O.each(chunks, (chunk) => {
                const utterance = new SpeechSynthesisUtterance(chunk)
                const voice = Speaker.getVoice()
                if (voice) {
                    console.log("Playing voice: " + voice.name)
                    utterance.voice = voice
                }
                utterance.volume = 1
                utterance.rate = 0.9
                utterance.pitch = 1
                Speaker.synthesis.speak(utterance)
            })
        }
    }

    /**
     * Stop speaking all queued text.
     */
    static stop() {
        if (Speaker.available) {
            Speaker.synthesis.cancel()
        }
    }

    static getVoice(): SpeechSynthesisVoice | null {
        const splitRegex = /[\s\-]+/
        const voices = Speaker.synthesis.getVoices()
        const keywords = ["uk", "english", "kingdom", "en", "eng"]
        return O.find(voices, (voice) => voice.name === "Google UK English Female")
            || O.find(voices, (voice) => voice.name === "English United Kingdom")
            || O.find(voices, (voice) => {
                const words = O.lower(voice.name).split(splitRegex)
                return O.some(words, (word) => O.some(keywords, O.is(word)))
            })
    }
}
